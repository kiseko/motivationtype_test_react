import React, {Component} from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux'

class App extends Component {
  render() {
    return(
    )
  }
}

function mapStateToProps(state) {
  return state
}

export default withCookies(connect(mapStateToProps)(App));
